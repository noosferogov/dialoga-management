# Primeiros Passos

Antes de começar, certifique-se de que possui os [requisitos](https://gitlab.com/participa/dialoga-management/tree/master#requisitos).

1. Clone este projeto: `git clone`
2. Instale as dependências: `npm install && bower install`
3. Inicie em modo desenvolvimento: `gulp serve`

# Referências

- Yeoman Generator: [generator-gulp-angular](https://github.com/Swiip/generator-gulp-angular)
  - Veja as tasks do gulp [aqui](https://github.com/Swiip/generator-gulp-angular#use-gulp-tasks)

# Requisitos

* Git: [como instalar](https://www.digitalocean.com/community/tutorials/como-instalar-o-git-no-ubuntu-14-04-pt)
* NodeJS: [como instalar](http://bfy.tw/do4) no Ubuntu 14.04
  * Bower: `npm install -g bower`
  * Gulp: `npm install -g gulp`
