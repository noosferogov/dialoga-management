(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .config(config)
    .config(configAuthInterceptor)
    .config(configTheme);

  /** @ngInject */
  function config($logProvider, toastr/*, $mdIconProvider*/) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastr.options.timeOut = 3000;
    toastr.options.positionClass = 'toast-top-right';
    toastr.options.preventDuplicates = true;
    toastr.options.progressBar = true;

    // $mdIconProvider
  }

  /** @ngInject */
  function configAuthInterceptor($httpProvider){

    //Reset headers to avoid OPTIONS request (aka preflight)
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};

    // $httpProvider.defaults.useXDomain = true;
    // $httpProvider.defaults.headers.common = {Accept: 'application/json, text/plain, */*'};
    // $httpProvider.defaults.headers.post = {'Content-Type': "application/json;charset=utf-8"};
    $httpProvider.defaults.headers.post = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'};

    $httpProvider.interceptors.push([
      '$injector',
      function ($injector) {
        return $injector.get('AuthInterceptor');
      }
    ]);
  }

  /** @ngInject */
  function configTheme($mdThemingProvider){
    $mdThemingProvider.definePalette('amazingPaletteName', {
      '50': 'ffebee',
      '100': 'ffcdd2',
      '200': 'ef9a9a',
      '300': 'e57373',
      '400': 'ef5350',
      '500': 'f44336',
      '600': 'e53935',
      '700': 'd32f2f',
      '800': 'c62828',
      '900': 'b71c1c',
      'A100': 'ff8a80',
      'A200': 'ff5252',
      'A400': 'ff1744',
      'A700': 'd50000',
      'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                          // on this palette should be dark or light
      'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
       '200', '300', '400', 'A100'],
      'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });
    $mdThemingProvider.theme('default')
      // .primaryPalette('red')
      // .primaryPalette('blue')
      // .primaryPalette('pink')
      // .primaryPalette('amazingPaletteName')
      // .dark()
      ;
  }

})();
