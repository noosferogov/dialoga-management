(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .run(runAuth)
    .run(runBlock);

  /** @ngInject */
  function runAuth($rootScope, $cookies, USER_ROLES, AUTH_EVENTS, AuthService, $log){

    $rootScope.currentUser = $cookies.get('user') || null;
    $rootScope.userRoles = USER_ROLES;
    $rootScope.userToken = $cookies.get('userToken');
    $rootScope.isAuthenticated = AuthService.isAuthenticated;
    $rootScope.isAuthorized = AuthService.isAuthorized;

    if($rootScope.currentUser){
      $rootScope.currentUser = JSON.parse($rootScope.currentUser);
    }

    $rootScope.setCurrentUser = function (user) {
      $rootScope.currentUser = user;
      $cookies.put('user', JSON.stringify(user));
    };

    // Listner url/state changes, and check permission
    $rootScope.$on('$stateChangeStart', function (event, next) {
      if(!next.data || !next.data.authorizedRoles){
        $log.debug('public url/state');
        return;
      }

      var authorizedRoles = next.data.authorizedRoles;
      if (!AuthService.isAuthorized(authorizedRoles)) {
        event.preventDefault();
        if (AuthService.isAuthenticated()) {
          // user is not allowed
          $log.debug('user is not allowed');
          $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
        } else {
          // user is not logged in
          $log.debug('user is not logged in');
          $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        }
      }
    });

    $rootScope.$on(AUTH_EVENTS.loginSuccess, function(e, user){
      $rootScope.setCurrentUser(user);
    });

    $rootScope.$on(AUTH_EVENTS.logoutSuccess, function(/*e*/){
      $rootScope.setCurrentUser(null);
    });

    $log.debug('runAuth end');
  }

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
