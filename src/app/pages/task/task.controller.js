(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .controller('TaskController', TaskController)
    .filter('onlyUnverified', onlyUnverified);

  /** @ngInject */
  function TaskController($timeout, $scope, TaskService) {
    var vm = this;
    // var imagePath = "//www.gravatar.com/avatar/5c7f86164c25bf9a0c9d6c38088369a8?only_path=false&amp;size=50&amp;d=wavatar";

    TaskService.getArticles().then(function(data){
      var tasks = data.articles;

      // adapter
      angular.forEach(tasks, function (value/*, key*/) {
        var task = value;
        task.evaluation = null;
        task.tagListReadOnly = false;
        task.published = false;
      });

      vm.propostas = tasks;
    });

    vm.responsibles = [{
        value: '',
        name: 'Todos'
    }];
  }

  function onlyUnverified () {
    return function (array) {
        var result = [];

        angular.forEach(array, function (value/*, key*/) {
          if(!value.saved){
            result.push(value);
          }
        });

        return result;
    };
  }
})();
