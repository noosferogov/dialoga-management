(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .controller('AuthController', AuthController);

  /** @ngInject */
  function AuthController($scope, $rootScope, $state, AUTH_EVENTS, AuthService) {
    var vm = this;

    vm.credentials = {
      username: '',
      password: ''
    };

    vm.logout = function () {
      AuthService.logout();
      $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    };

    vm.login = function (credentials) {
      AuthService.login(credentials).then(function (user) {
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, user);
        $state.go('task');
      }, function () {
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
      });
    };

  }
})();
