/* global malarkey:false, toastr:false, moment:false */
(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .constant('private_token', '375bee7e17d0021af7160ce664874618')
    .constant('malarkey', malarkey)
    .constant('toastr', toastr)
    .constant('moment', moment)
    .constant('AUTH_EVENTS', {
      loginSuccess: 'auth-login-success',
      loginFailed: 'auth-login-failed',
      logoutSuccess: 'auth-logout-success',
      sessionTimeout: 'auth-session-timeout',
      notAuthenticated: 'auth-not-authenticated',
      notAuthorized: 'auth-not-authorized'
    })
    .constant('USER_ROLES', {
      all: '*',
      admin: 'admin',
      editor: 'editor',
      guest: 'guest'
    })
    .constant('DIALOGA_ROLES', {
      all: '*',
      perform_task: 'perform_task'
    })
    ;

})();
