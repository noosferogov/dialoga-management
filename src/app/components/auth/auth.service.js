(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .service('Session', SessionService)
    .factory('AuthService', AuthService)
    .factory('AuthResolver', AuthResolver)
    .factory('AuthInterceptor', AuthInterceptor);

  /** @ngInject */
  function AuthService($http, $location, Session){
    var service = {};
    var host = 'http://www.participa.br';

    service.login = function (credentials) {
      var loginUrl = host + '/api/v1/login';
      var encodedData = 'login=' + credentials.username + '&password=' + credentials.password;

      return $http
        .post(loginUrl, encodedData)
        .then(function (res) {
          res.data.userRoles = ['admin'];
          Session.create(res.data.id, res.data.person.id, res.data.userRoles, res.data.private_token);
          return res.data;
        });
    };

    service.logout = function () {
      // var logoutUrl = host + '/api/v1/logout', encodedData;
      Session.destroy();
      // $location.path('/');
    };

    service.isAuthenticated = function () {
      return !!Session.userId;
    };

    service.isAuthorized = function (authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }

      return (service.isAuthenticated() &&
        authorizedRoles.indexOf(Session.userRoles) !== -1);
    };

    return service;
  }

  /** @ngInject */
  function SessionService($cookies){

    this.id = $cookies.get('id');
    this.userId = $cookies.get('userId');
    this.userRoles = $cookies.get('userRoles');
    this.userToken = $cookies.get('userToken');

    this.create = function (sessionId, userId, userRoles, userToken) {
      this.id = sessionId;
      this.userId = userId;
      this.userRoles = userRoles;
      this.userToken = userToken;

      $cookies.put('id', sessionId);
      $cookies.put('userId', userId);
      $cookies.put('userRoles', userRoles);
      $cookies.put('userToken', userToken);
    };

    this.destroy = function () {
      this.id = null;
      this.userId = null;
      this.userRoles = null;
      this.userToken = null;

      $cookies.remove('id');
      $cookies.remove('userId');
      $cookies.remove('userRoles');
      $cookies.remove('userToken');
    };
  }

  /** @ngInject */
  function AuthInterceptor($rootScope, $q, AUTH_EVENTS){
    return {
      responseError: function (response) {
        $rootScope.$broadcast({
          401: AUTH_EVENTS.notAuthenticated,
          403: AUTH_EVENTS.notAuthorized,
          419: AUTH_EVENTS.sessionTimeout,
          440: AUTH_EVENTS.sessionTimeout
        }[response.status], response);
        return $q.reject(response);
      }
    };
  }

  /** @ngInject */
  function AuthResolver($q, $rootScope, $state){
    return {
      resolve: function () {
        var deferred = $q.defer();
        var unwatch = $rootScope.$watch('currentUser', function (currentUser) {
          if (angular.isDefined(currentUser)) {
            if (currentUser) {
              deferred.resolve(currentUser);
            } else {
              deferred.reject();
              $state.go('login');
            }
            unwatch();
          }
        });
        return deferred.promise;
      }
    };
  }

})();
