(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .directive('task', task);

  /** @ngInject */
  function task() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/task/task.html',
      scope: {
        task: '=task'
      },
      controller: TaskController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function TaskController(TaskService, $timeout, $log) {
      var vm = this;
      var DELAY = 1000; // ms

      if(!vm.task){
        vm.task = TaskService.mockTask();
      };

      vm.options = [
        { value: 'accept', label: 'Aceitar' },
        { value: 'refuse', label: 'Rejeitar' }
      ];

      vm.refuseOptions = [
        { value: 'reason1', label: 'Motivo 1' },
        { value: 'reason2', label: 'Motivo 2' },
        { value: 'reason3', label: 'Motivo 3' }
      ];

      vm.clearValues = function (item) { // proposal
        $log.warn('TODO: reset data of', item);
      };

      vm.save = function (item) { // proposal
        item.isLoading = true;
        $timeout(function(){
          item.saved = true;
        }, DELAY);
        $log.warn('TODO: save data of', item);
      };

      vm.publish = function (item) { // proposal
        item.published = true;
        $log.warn('TODO: publish data of', item);
      };

      vm.unpublish = function (item) { // proposal
        item.published = false;
        $log.warn('TODO: unpublish data of', item);
      };
    }
  }

})();
