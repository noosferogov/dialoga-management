(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .factory('TaskService', TaskService);

  /** @ngInject */
  function TaskService($http, $q, private_token, ErrorService, $log) {
    var apiHost = 'http://login.dialoga.gov.br/api/v1';
    var apiArticles = apiHost + '/articles?private_token=' + private_token + '&fields=id,children,created_by,categories,tag_list,abstract,setting,profile&content_type=ProposalsDiscussionPlugin::Proposal';
    var apiTasks = apiHost + '/task?private_token=' + private_token + '&fields=id,children,created_by,categories,tag_list,abstract,setting,profile&content_type=ProposalsDiscussionPlugin::Proposal';

    var service = {
      // mock
      mockTask: mockTask,
      // api
      getArticles: getArticles,
      getTask: getTask,
      getTasks: getTasks
    };

    return service;

    // ---
    // PUBLIC METHODS
    // ---

    /**
     * Mock Task
     * @return {Object} a new Task with default data;
     */
    function mockTask () {
      return {
        id: -1,
        abstract: '[abstract]',
        author: '[author]',
        categories: [], // saude, educacao, etc.
        children: [], // ?
        profile: { // ?
          id: -1,
          identifier: '[profile.identifier]',
          name: '[profile.name]',
        },
        setting: { // ?
          author_name: '[setting.author_name]',
          comment_paragraph_plugin_activate: false
        },
        tag_list: []
      };
    }

    /**
     * Get a list of articles
     * @param  {Number} limit per_page (default is 30)
     * @return {Array} a list of articles
     */
    function getArticles (limit) {
      if (!limit) {
        limit = 30;
      }

      return $http.get(apiArticles)
        .then(handleSuccess)
        .catch(handleError);
    }

    /**
     * Get a task by id
     * @param  {Number} id of task
     * @return {Object} the wanted task or a new one.
     */
    function getTask (id) {
      if (!id) {
        throw new Error(ErrorService.paramRequired('id'));
      }

      return $http.get(apiTasks)
        .then(handleSuccess)
        .catch(handleError);
    }

    /**
     * Get a list of tasks
     * @param  {Number} limit per_page (default is 30)
     * @return {Array} a list of tasks
     */
    function getTasks (limit) {
      if (!limit) {
        limit = 30;
      }

      return $http.get(apiTasks)
        .then(handleSuccess)
        .catch(handleError);
    }

    // ---
    // PRIVATE METHODS
    // ---

    /**
     * Transform the successful response, unwrapping the application data
     * from the API response payload.
     *
     * @param  {Object} response from the server.
     * @return {Object}          the data unwrapped.
     */
    function handleSuccess (response){
      return response.data;
    }

    /**
     * Transform the error response, unwrapping the application data from
     * the API response payload.
     *
     * @param  {Object} error from the server.
     * @return {Promise}      promise rejection called.
     */
    function handleError (error){

      $log.error('XHR Failed on TaskService.\n' + angular.toJson(error.data, true));

      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if ( !angular.isObject( error.data ) || !error.data.message) {
        return( $q.reject( 'An unknown error occurred.' ) );
      }

      // Otherwise, use expected error message.
      return $q.reject(error.data.message);
    }
  }
})();
