(function() {
  'use strict';

  describe('A task directive (<task>)', function(){

    var $httpBackend, $compile, $rootScope, element;

    // Load the module, which contains the directive
    beforeEach(function(){
      module('dialoga-admin-app');
      module('ngMockE2E');
    });

    // Store references to $rootScope and $compile
    // so they are available to all tests in this describe block
    beforeEach(inject(function(_$httpBackend_, _$compile_, _$rootScope_){
      // The injector unwraps the underscores (_) from around the parameter names when matching
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;

      $httpBackend.whenGET('/assets/images/icons/plus.svg').respond('');

      // Compile a piece of HTML containing the directive
      element = $compile('<task></task>')($rootScope.$new());

      // fire all the watches, so the scope expression {{1 + 1}} will be evaluated
      $rootScope.$digest();
    }));

    it('must have an author name', function() {
      var author_name = element.find('.proposal__author_name');
      expect(author_name).not.toBeNull();
      expect(author_name.length).toEqual(1);
    });

    it('must have an abstract', function() {
      var abstract = element.find('.proposal__abstract');
      expect(abstract).not.toBeNull();
      expect(abstract.length).toEqual(1);
    });

    it('must have a details', function() {
      var details = element.find('.proposal__details');
      expect(details).not.toBeNull();
      expect(details.length).toEqual(1);
    });

    it('must have a tag-list', function() {
      var tagList = element.find('.proposal__tag-list');
      expect(tagList).not.toBeNull();
      expect(tagList.length).toEqual(1);
    });
  });
})();
