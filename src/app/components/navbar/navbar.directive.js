(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      transclude: true,
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($mdSidenav, $mdUtil) {
      var vm = this;

      vm.toggleNav = buildToggler('left');

      // $scope.toggleLeft = buildToggler('left');
      // $scope.toggleRight = buildToggler('right');

      /**
       * Build handler to open/close a SideNav; when animation finishes
       * report completion in console
       */
      function buildToggler(navID) {
        var debounceFn =  $mdUtil.debounce(function(){
          $mdSidenav(navID)
            .toggle()
            .then(function () {
              // $log.debug("toggle " + navID + " is done");
            });
        },300);

        return debounceFn;
      }
    }
  }

})();
