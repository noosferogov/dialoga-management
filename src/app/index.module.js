(function() {
  'use strict';

  angular
    .module('dialoga-admin-app', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ngMaterial', 'ngMessages']);

})();
