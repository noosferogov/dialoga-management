(function() {
  'use strict';

  angular
    .module('dialoga-admin-app')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider, USER_ROLES) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/pages/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        // data: {
        //   authorizedRoles: [USER_ROLES.all]
        // },
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/pages/auth/login.html',
        controller: 'AuthController',
        controllerAs: 'auth',
        // data: {
        //   authorizedRoles: [USER_ROLES.all]
        // }
      })
      .state('task', {
        url: '/task',
        templateUrl: 'app/pages/task/taskList.html',
        controller: 'TaskController',
        controllerAs: 'task',
        authorizedRoles: [USER_ROLES.admin, USER_ROLES.editor],
        resolve: {
          auth: function resolveAuthentication(AuthResolver) {
            return AuthResolver.resolve();
          }
        }
      });

    $urlRouterProvider.otherwise('/');
  }

})();
